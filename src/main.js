import App from './App.svelte';

export default async (loader) => {
	const data = await loader.load('example:data');

	return ({ target }) => {
		const app = new App({
			target,
			props: {
				data
			}
		});

		return app;
	}
};