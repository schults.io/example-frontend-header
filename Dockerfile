FROM node:14-alpine

WORKDIR /app

COPY . /app/

RUN ls -la

RUN npm ci \
    && npm run build \
    && mkdir /data \
    && cp -r ./dist/* /data/